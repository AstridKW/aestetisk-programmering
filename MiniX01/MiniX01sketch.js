
function setup() {
 // put setup code here
 createCanvas(windowWidth, windowHeight);
 background(213, 239, 166);
 textSize(32);
  fill(236, 61, 107);
  strokeWeight(4);
 text('plingeling', 100, 100);
 
}

function draw() {
  // put drawing code here
  noStroke();
  ellipse(250, 250, 55, 55);
  fill(235, 204, 476); // pink
  ellipse(260, 190, 55, 55);
  ellipse(190, 230, 55, 55);
  ellipse(205, 300, 55, 55);
  ellipse(270, 310, 55, 55);
  ellipse(310, 250, 55, 55);
  fill(255, 255, 153); // yellow

  triangle(880, 320, 940, 220, 1000, 320);

  stroke(0);
  line(910, 270, 880, 220); // Left arm
  line(970, 270, 1000, 220); // Right arm
  line(910, 320, 910, 430); // Left leg
  line(970, 320, 970, 430); // Right leg

  let img = createImg("https://media0.giphy.com/media/TGX3d4A6negItDUpK6/giphy.gif?cid=6c09b9523el0rz0656vifzcbsm21xl7zkxlfkkkfawpge2bp&ep=v1_internal_gif_by_id&rid=giphy.gif&ct=s");
  img.position(350,100);
  img.size(200,200)

  let img1 = createImg("https://png.pngtree.com/png-vector/20220729/ourmid/pngtree-cute-bear-head-cartoon-png-image_6091974.png");
  img1.position(875,120);
  img1.size(130,130)

  // Stars
  frameRate(10);
  let x = random(width);
  let y = random(height);
  textSize(20); //Star size
  text('⭐', x, y);

}

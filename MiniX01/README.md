### What have you produced?
I have produced ellipses that form a flower, some text; I have put a GIF on the canvas and made a little stick figure consisting of a triangle, four lines, and an image. I have also made stars appear at random on the page! My code is not very aesthetic and does not have any interactive elements, but I wish to focus on these things in the future MiniX’s. This time I tried to get familiar with p5.js.
![](minix01.png)

### How would you describe your first independent coding experience (in relation to thinking, reading, copying, modifying, writing code, and so on)?
I’ve enjoyed this coding experience, and I’ve found the references to be immensely helpful. The ones I’ve looked at also appear understandable to me. It has also been really fun so far and I’m excited to get better. 

### How is the coding process different from, or similar to, reading and writing text?
Similar to reading text, coding involves interpreting and understanding. In terms of writing, coding requires a precise and structured approach unlike "text writing," where ambiguity may be valued at times. Although both follow syntax rules. Another thing I notice is that for example when writing in Google Docs, the program will tell me if I misspell something, whereas coding programs are not exactly as helpful when making a mistake.

### What does code and programming mean to you, and how does the assigned reading help you to further reflect on these terms?
To me, coding and programming are creative expressions that I’m excited to get better at. It’s also a lot of problem-solving, which can be frustrating but giving once solved. The reading refreshes terms we’ve learned in the previous lessons, making some of them clearer to me.


### References
https://p5js.org/reference/#/p5/ellipse

https://p5js.org/reference/#/p5/image

https://p5js.org/reference/#/p5/line

https://p5js.org/reference/#/p5/random

https://p5js.org/reference/#/p5/triangle

Soon Winnie & Cox, Geoff, "Getting started", Aesthetic Programming: A Handbook of Software Studies, London: Open Humanities Press, 2020, pp. 27-48
